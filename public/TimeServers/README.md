### Default port
Default NTP Server UDP port - 123

Default NIST Server TCP port - 13

### Info
The NIST protocol is very expensive in terms of network bandwidth, since it uses the complete tcp machinery to transmit only 32 bits of data. Users are encouraged to upgrade to the network time protocol (NTP), which is both more accurate and more robust.
It is a bad practice to "hard-code" a particular server name or address.
All users should ensure that their software NEVER queries a server more frequently, some consider it as a denial-of-service attack.

### Sources
https://gist.github.com/mutin-sa/eea1c396b1e610a2da1e5550d94b0453

https://tf.nist.gov/tf-cgi/servers.cgi
